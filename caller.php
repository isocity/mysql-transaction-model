<!DOCTYPE html>
<html lang="de">

	<head>
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
		<title id="title">isocity</title>
		<meta name="theme-color" content="#111111" id="meta-theme-color" />
		<meta name="msapplication-TileColor" content="#111111" id="meta-msapplication-TileColor" />
		<meta name="description" content="isocity | apps" id="meta-description" />
		<meta name="keywords" content="isocity, apps, cloud, webapps" id="meta-keywords" />
		<meta name="author" content="isocity" id="meta-author" />
		<meta name="robots" content="index, follow" id="meta-robots" />


		<link rel="shortcut icon" href="theme/favicon.ico" type="image/x-icon" />
		<style>
			
			html{
				height: 24px;
				width: 100%;
			}
			body{
				height: 100%;
				width: 100%;
				margin: 0;
				border-bottom: 1px solid black;
				font-family: verdana;
			}
						
		</style>

	</head>
	
	<body class="body">
		
		<input type="text" id="http" value="http://" style="padding: 2px 4px; font-size: 8pt; background: white; color: gray; width: 800px;" />
		<input type="button" value="Start" style="border: 1px solid gray; background: white; cursor: pointer;" onclick="start()">
		
		<script>			
			
			var start = function(){
				var http = document.getElementById('http').value;
				for(var i = 1; i <= 10; i++){
					window.top.frames['test'+i].location.href = http;
				}
			}
		
		</script>
		
	</body>
	
</html>
