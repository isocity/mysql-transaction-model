<?php

	function debug($str){
		echo '<div style="font-family: \'courier new\'; font-size: 8pt; color: gray; padding: 12px 16px; border: 1px solid silver; background-color: #eeeeee; border-radius: 5px; ">'.$str.'</div> ';
	}
	function secure_sql($string){
		global $token;
		return mysqli_real_escape_string($token['connection'],str_replace('--','[-]',$string));
	}

//preset
	$tbl = '___home_impressum_wordcascade_wordcascade';
	
	$id = date('YmdHis'); //start id
	
	$config = array(
		'sql' => array(
			'server' => 'localhost',
			'user' => 'root',
			'password' => '',
			'database' => 'webgen_tkr'
		)
	);
	
	$token = array(
		'connection' => false
	);
	
//SQL session start
	$token['connection'] = mysqli_connect(
		$config['sql']['server'],$config['sql']['user'],
		$config['sql']['password'],$config['sql']['database']
	) or die(mysqli_connect_error());	

	
//SET fields
	$ft = '`headline` text,`word` text';
	$fields = array('`headline`','`word`');
	$values = array('\'\'','\'\'');
	
//make table
	$query = "CREATE TABLE IF NOT EXISTS `".secure_sql($tbl)."` (
		`id` double NOT NULL,
		".$ft." ,
		`unlocked` tinyint(1) NOT NULL,
		`order` int(255),
		`timestamp` datetime
	)";
	if(!mysqli_query($token['connection'], $query)){die($query.' Table '.$tbl.' could not be created');}
	$query = "ALTER TABLE `".secure_sql($tbl)."` ADD PRIMARY KEY (`id`);";
	mysqli_query($token['connection'], $query);
	
	
//fill up	
	$query = "LOCK TABLES `".secure_sql($tbl)."` write;"; //LOCK
	if(mysqli_query($token['connection'],$query)){
		
		$query = "SELECT MAX(`order`) FROM `".secure_sql($tbl)."`;";
		$res = mysqli_query($token['connection'],$query);
		if($res){
			if($res!==TRUE && mysqli_num_rows($res)!=0){
				$res = mysqli_fetch_assoc($res);
				$order = $res['MAX(`order`)']+1;
			}else{
				$order = 1;
			}
			$query = "SELECT * FROM `".secure_sql($tbl)."` where id='".$id."' limit 1;";
			$res = mysqli_query($token['connection'],$query);
			if($res){
				if($res!==TRUE && mysqli_num_rows($res)!=0){//new id
					$res = mysqli_fetch_assoc($res);
					$id = date('YmdHis',strtotime($res['id'])+1);
					debug('new id:' . $id);
				}else{
					//id remains
				}
				
				$field_str = implode(',', $fields);
				$value_str = implode(',', $values);
				$field_str = "`id` ,".$field_str.", `unlocked`, `order`, `timestamp`";
				$value_str = "'".$id."' ,".$value_str.", 0, '".$order."', '".date('Y-m-d H:i:s')."'";
				
				$multiSQL = "
					INSERT INTO `".secure_sql($tbl)."` (".$field_str.") VALUES (".$value_str.");
					SELECT LAST_INSERT_ID();
				";
				$insert = mysqli_multi_query($token['connection'],$multiSQL);
				if($insert){
					if($insert!==TRUE){
						echo $insert;
					}else{
						debug('success:'.$multiSQL);
					}
				}else{
					debug('error:'.$multiSQL);
					mysqli_query($token['connection'],"UNLOCK TABLES `".secure_sql($tbl)."`;");
					die('error in `'.secure_sql($tbl).'` insert');
				}
				mysqli_query($token['connection'],"UNLOCK TABLES `".secure_sql($tbl)."`;");
				
			}else{
				mysqli_query($token['connection'],"UNLOCK TABLES `".secure_sql($tbl)."`;");
				die('error in `'.secure_sql($tbl).'` get id');
			}
			
		}else{
			mysqli_query($token['connection'],"UNLOCK TABLES `".secure_sql($tbl)."`;");
			die('error in `'.secure_sql($tbl).'` get max order');
		}
	}
	
//SQL session end
mysqli_close($token['connection']);

?>